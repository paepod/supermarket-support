name := "supermarket-support"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)     

play.Project.playJavaSettings

com.jamesward.play.BrowserNotifierPlugin.livereload
