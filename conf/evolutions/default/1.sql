# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table customer (
  id                        bigint not null,
  name                      varchar(255),
  customer_number           varchar(255),
  constraint pk_customer primary key (id))
;

create table employee (
  id                        bigint not null,
  name                      varchar(255),
  employee_number           varchar(255),
  constraint pk_employee primary key (id))
;

create table product (
  dtype                     varchar(10) not null,
  id                        bigint not null,
  name                      varchar(255),
  barcode                   varchar(255),
  kilos_in_stock            decimal(38),
  cents_per_kilo            integer,
  quantity_in_stock         integer,
  price_per_item            integer,
  constraint pk_product primary key (id))
;

create table role (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_role primary key (id))
;

create table sale (
  id                        bigint not null,
  employee_id               bigint,
  customer_id               bigint,
  created_at                timestamp,
  total_ex_tax              bigint,
  total_inc_tax             bigint,
  constraint pk_sale primary key (id))
;

create table sale_item (
  id                        bigint not null,
  sale_id                   bigint not null,
  product_id                bigint,
  quantity                  bigint,
  weight                    double,
  price_of_product          double,
  constraint pk_sale_item primary key (id))
;


create table employee_role (
  employee_id                    bigint not null,
  role_id                        bigint not null,
  constraint pk_employee_role primary key (employee_id, role_id))
;
create sequence customer_seq;

create sequence employee_seq;

create sequence product_seq;

create sequence role_seq;

create sequence sale_seq;

create sequence sale_item_seq;

alter table sale add constraint fk_sale_employee_1 foreign key (employee_id) references employee (id) on delete restrict on update restrict;
create index ix_sale_employee_1 on sale (employee_id);
alter table sale add constraint fk_sale_customer_2 foreign key (customer_id) references customer (id) on delete restrict on update restrict;
create index ix_sale_customer_2 on sale (customer_id);
alter table sale_item add constraint fk_sale_item_sale_3 foreign key (sale_id) references sale (id) on delete restrict on update restrict;
create index ix_sale_item_sale_3 on sale_item (sale_id);
alter table sale_item add constraint fk_sale_item_product_4 foreign key (product_id) references product (id) on delete restrict on update restrict;
create index ix_sale_item_product_4 on sale_item (product_id);



alter table employee_role add constraint fk_employee_role_employee_01 foreign key (employee_id) references employee (id) on delete restrict on update restrict;

alter table employee_role add constraint fk_employee_role_role_02 foreign key (role_id) references role (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists customer;

drop table if exists employee;

drop table if exists employee_role;

drop table if exists product;

drop table if exists role;

drop table if exists sale;

drop table if exists sale_item;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists customer_seq;

drop sequence if exists employee_seq;

drop sequence if exists product_seq;

drop sequence if exists role_seq;

drop sequence if exists sale_seq;

drop sequence if exists sale_item_seq;

