### OOSD Major Assignment
#### Supermarket Support

The group formerly known as Prince
----------------------------------

- Chris Atkins (s3229710)
- James Formica (s3331854)
- Shinnawat Viboonsitthichok (s3383917)
- Marlon Atton (s3317517)

Notes to marker
---------------

*Kindly note that Marlon Atton contributed 2 / 369 commits to the project*

### Basic architechture

- Play Framework 2.2 Application
    - Serving server side pages
    - RESTful JSON API
    - Database interactions via Ajave Ebean
    - Serving compiled LESS, CSS, JavaScript and CoffeeScript assets

- 3 AngularJS applications
    - Administration section
    - Point of Sales section
    - Reporting section

The AngularJS applications interact with the play framework backend via RESTful JSON calls, based elements from the following API design philosphies.
They are each divided into their own modules, and could be deployed independently of the server side application. They are decoupled from the server. If this application was to grow, it could migrate across server platforms, programming languages, adopt service oriented architecture etc. with few if any changes to the client applications. Likewise, the client side applications can change and be created independently of the server.

The existing REST API could be used as the data source for a native mobile or desktop application, or integrate with other applications with ease.

- [Heroku HTTP API Design Guide](https://github.com/interagent/http-api-design)
- [json:api](http://jsonapi.org)

The application was designed to be in line with modern development practices eg.

- Git for version control
- JSON for data exchange
- Using a CSS preprocessor (in this case [LESS](http://lesscss.org/))
- Resource based RESTful API
- Stateless Server
- Modular Javascript
- Components for reusable view elements
- BitBucket Pull requests for code review
- Frequent refactorings based on code review feedback and general code hygiene

### Project Structure

(as at 4a91b29)

    ├── app                           # main logic lives here
    │   ├── assets
    │   │   ├── javascripts
    │   │   │   ├── Sales             # front end app for point of sales
    │   │   │   ├── admin             # front end app for administation
    │   │   │   ├── common
    │   │   │   ├── report            # front end app for reporting
    │   │   │   └── shared            # shared angularJS resources
    │   │   └── stylesheets           # css for the application
    │   ├── controllers               # root level controllers
    │   │   ├── api                   # restful api controllers
    │   │   └── authorization         # package for auth middlewares
    │   ├── models                    # domain models
    │   │   └── utils                 # some utilities created by Shinnawat
    │   │       ├── extractStrategies # strategies for extracting json
    │   │       ├── mappers           # mapping between request and viewmodels
    │   │       └── reports
    │   ├── viewModels                # non database backed models
    │   └── views
    │       ├── admin                 # views for the administration application
    │       ├── components            # shared template components
    │       ├── reporting             # views for the reporting section
    │       └── sales                 # views for the sales section
    │           └── partials
    ├── conf                          # configuration and initial data
    │   └── evolutions
    │       └── default               # generated database schema file
    ├── project                       # build configuration for sbt
    └── public                        # vendored front end dependencies
        ├── fonts
        ├── images
        ├── javascripts
        └── stylesheets

Tree generated using unix command `tree -d -I target` and manually adding notes. If this goes out of sync update it.

Development notes
=================

This project will use the [Git flow](http://nvie.com/posts/a-successful-git-branching-model/) branching strategy.

Here is another article about [Git flow](https://www.atlassian.com/git/workflows#!workflow-gitflow) from Atlassian.


### Frameworks used

- [Play! Framework](http://www.playframework.com/) v2.2
- [AngularJS](https://angularjs.org/)
- [RestAngular](https://github.com/mgonto/restangular)
- [Toaster](https://github.com/jirikavi/AngularJS-Toaster)
- [Twitter Bootstrap](http://getbootstrap.com/)

*(Don't install through typesafe activator, because that will drag along heaps of stuff that you don't need)*

### Running the application

After you've got play installed, verify that the `play` executable is available in your `$PATH` (*nix) or `%PATH%` (windows)

    $ play
    play 2.2.2 built with Scala 2.10.3 (running Java 1.6.0_65), http://www.playframework.com

    > Type "help play" or "license" for more information.
    > Type "exit" or use Ctrl+D to leave this console.

This opens up the play console. From here, use the `run` command to fire up the app. The first time, this might take a while, as it has to gather required dependencies.

    [supermarket-support] $ run
    [info] Updating {file:/Users/christopheratkins/Developer/uni/supermarket-support/}supermarket-support...
    [info] Resolving org.fusesource.jansi#jansi;1.4 ...
    [info] Done updating.

    --- (Running the application from SBT, auto-reloading is enabled) ---

    [info] play - Listening for HTTP on /0:0:0:0:0:0:0:0%0:9000

    (Server started, use Ctrl+D to stop and go back to the console...)

    [info] Compiling 4 Scala sources and 2 Java sources to /Users/christopheratkins/Developer/uni/supermarket-support/target/scala-2.10/classes...
    [info] play - Application started (Dev)

If you've gotten this far, the app is running at [http://localhost:9000](http://localhost:9000). It will most likely ask you to apply database evolutions. Do that, then you're away!

### Debugging

You can also fire up your the app with a remote debugger running which is really handy.

    $ play debug

Or to start `play` and run straight away..

    $ play debug ~run

For more info check out the [Setting up your IDE](http://www.playframework.com/documentation/2.2.x/IDE) page in the documentation which tells you how to set it up.

### Database

The app is currently set up to use the Java base H2 database, set to `In Memory` mode. The upshot of this is, when you shut down the app, your data goes with it.

When the application is in development mode, it loads up some fixture data from [`conf/initial-data.yml`](supermarket-support/src/develop/conf/initial-data.yml). This is hooked up in [`app/Global.java`](supermarket-support/src/develop/app/Global.java).

### Live Reload

There is a [live reload plugin ](https://github.com/jamesward/play-auto-refresh) installed. For this to work, you must install the [Play Framework Tools](https://chrome.google.com/webstore/detail/play-framework-tools/dchhggpgbommpcjpogaploblnpldbmen) Chrome extension. Basically changes to the code will refresh the browser which is nice.

### Play Framework Documentation

Play includes local documentation, and serves is when you're running the app in development mode.

There are some handy ones in there.

- [Play for Java developers](http://localhost:9000/@documentation/JavaHome) (local)
- [Todo list tutorial](http://localhost:9000/@documentation/JavaHome) (local)
- [ZenTasks](http://localhost:9000/@documentation/JavaGuide1) (local)

The Zentasks one is a bit more thorough, and runs through most of the play framework concepts.

### Handly Links

- [AngularJS docs](https://docs.angularjs.org/api)
- [Egghead AngularJS Tutorials](https://egghead.io/technologies/angularjs) (mostly free videos)
- [Day 30: Play Framework--A Java Developer Dream Framework](https://www.openshift.com/blogs/day-30-play-framework-a-java-developer-dream-framework) (OpenShift blog)

_Sidenote: To update your feature branches follow this workflow_

    $ git checkout develop # Checkout the latest version of develop
    $ git pull origin develop # Pull the remote changes into your copy of develop
    $ git checkout feature/awesome-branch # Switch back to your feature branch
    $ git merge develop # Merge the changes into your copy
