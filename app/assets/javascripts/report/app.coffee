###
  Report Applications
  Created by Shinnawat Viboonsitthichok on 10/05/2014.
###
@adminApp = app = angular.module 'reportApp', [
  'restangular',
  'report.controllers'
]

app.config (RestangularProvider) ->
  RestangularProvider.setBaseUrl '/api'