###
  Controllers module for Report application
  Created by Shinnawat Viboonsitthichok on 10/05/2014.
###
app = angular.module('report.controllers', ['restangular'])

app.controller 'ReportItemListCtrl', ($scope, $log, Restangular) ->
  baseReportItems = Restangular.all('reports')

  resetForm = () -> $scope.searchForm =  {}

  $scope.searchForm = {}

  baseReportItems.getList().then (reportItems) ->
    $scope.reportItems = reportItems

  $scope.resetSearchForm = () ->
    resetForm()

  $scope.search = (searchForm) ->
    baseReportItems.post(searchForm).then (reportItems) ->
      $scope.reportItems = reportItems
    .catch (error) ->
      $log.error(error)