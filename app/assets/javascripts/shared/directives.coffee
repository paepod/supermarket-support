###
  Shared directives for use in any of the Angular JS Application
###
app = angular.module('shared.directives', [])

# <glyphicon> directive to render bootstrap icons
app.directive 'glyphicon', ->
  restrict: 'E'
  template: '<span class="glyphicon" ng-class="iconClass"></span>'

  link: (scope, elem, attrs) ->
    scope.iconClass = "glyphicon-#{scope.icon}"

  scope:
    icon: '@'
