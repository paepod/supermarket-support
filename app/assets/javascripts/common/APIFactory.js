/**
 * Created by James on 23/05/2014.
 */

angular.module('APIServices', ['restangular'])


    .factory('customerAPI', [ 'Restangular', function (Restangular) {

        var customersRoute = 'customers';
        var findCustomersRoute = 'customers/find';

        var searchForCustomer = function(criteria) {
            if (!isNaN(criteria)) {
                return Restangular.one(customersRoute, parseInt(criteria)).get();
            } else {
                return Restangular.one(findCustomersRoute, criteria).get();
            }
        };
        return {
            searchForCustomer : searchForCustomer
        };
    }])


    .factory('productAPI', ['Restangular', '$q', function (Restangular, $q) {

        var productsRoute = 'products';

        var getAllProducts = function() {
           return Restangular.all(productsRoute).getList();
        };

        var updateProductStock = function(productId, amountToDeduct) {
            return Restangular.one(productsRoute, productId).get().then(function(product) {
                if (product.productType.toLowerCase() === productTypes.quantity) {
                    product.quantityInStock -= amountToDeduct;
                } else {
                    product.kilosInStock -= amountToDeduct;
                }
                return product.put();
            });
        };

        var deductStockFromSale = function(saleItems) {
            var updates = saleItems.map(function(item) {
                var amountToDeduct = item.quantity ? item.quantity : item.weight;
                return updateProductStock(item.product.id, amountToDeduct);
            });

            return $q.all(updates);
        };

        return {
            getAllProducts : getAllProducts,
            deductStockFromSale: deductStockFromSale
        };
    }])


    .factory('employeeAPI', ['Restangular', function (Restangular) {

        var employeesRoute = 'employees';
        var currentEmployeeRoute = 'current';

        var getEmployee = function(id) {
            return Restangular.one(employeesRoute, id).get();
        };

        var getCurrentEmployee = function() {
            return Restangular.one(employeesRoute, currentEmployeeRoute).get();
        };

        return {
            getEmployee : getEmployee,
            getCurrentEmployee : getCurrentEmployee
        }
    }])


    .factory('saleAPI', ['Restangular', function (Restangular) {

        var salesRoute = 'sales';

        var postSale = function(sale) {
            return Restangular.all(salesRoute).post(sale);
        };

        return {
            postSale : postSale
        }
    }])

;
