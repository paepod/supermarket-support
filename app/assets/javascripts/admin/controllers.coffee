###
  Controllers module for administration application

  Created by Chris Atkins
###

app = angular.module('admin.controllers', ['restangular', 'toaster'])

app.controller 'ProductListCtrl', ($scope, $log, Restangular, toaster) ->
  $scope.productTypes = [
    'QUANTITY'
    'WEIGHTED'
  ]

  resetForm = () ->
    $scope.currentProduct = { productType: 'QUANTITY' }
    $scope.productForm.$setPristine()

  $scope.$watch 'currentProduct.productType', (newValue) ->
    product = $scope.currentProduct
    switch newValue
      when 'QUANTITY'
        delete product.centsPerKilo
        delete product.kilosInStock
        $scope.isWeightedProduct = false

      when 'WEIGHTED'
        delete product.pricePerItem
        delete product.quantityInStock
        $scope.isWeightedProduct = true

  $scope.currentProduct = { productType: 'QUANTITY' }

  baseProducts = Restangular.all('products')

  baseProducts.getList().then (products) ->
    $scope.products = products

  $scope.isPersisted = (product) ->
    product.id?

  $scope.editProduct = (product) ->
    $scope.currentProduct = product

  $scope.resetProduct = () ->
    resetForm()
    return false

  createProduct = (product) ->
    baseProducts.post(product)
    .then (p) -> $scope.products.push(p)

  updateProduct = (product) ->
    product.save()

  $scope.saveProduct = (product) ->
    save = if $scope.isPersisted(product)
      updateProduct(product)
    else
      createProduct(product)

    save.then ->
      toaster.pop 'success', 'Success!', 'Product saved'
    .catch (error) ->
      toaster.pop 'error', 'Uh oh!', 'A server error occurred.'
      $log.error(error)
    .finally () ->
      resetForm()


  $scope.deleteProduct = (product) ->
    product.remove().then ->
      $scope.products = _.without($scope.products, product)

app.controller 'EmployeeListCtrl', ($scope, $log, Restangular, toaster) ->
  baseEmployees = Restangular.all 'employees'

  baseRoles = Restangular.all 'roles'

  $scope.showForm = false
  $scope.currentEmployee = { roles: [] }

  baseRoles.getList().then (roles) ->
    $scope.roles = _.map roles, Restangular.stripRestangular

  baseEmployees.getList().then (employees) ->
    $scope.employees = employees

  $scope.isPersisted = (employee) ->
    employee.id?

  $scope.resetForm = ->
    $scope.showForm = false
    $scope.currentEmployee = { roles: [] }
    $scope.employeeForm.$setPristine()

  $scope.editEmployee = (employee) ->
    $scope.currentEmployee = employee
    $scope.showForm = true

  createEmployee = (employee) ->
    baseEmployees.post(employee)
    .then (p) -> $scope.employees.push(p)

  updateEmployee = (employee) ->
    employee.save()

  $scope.saveEmployee = (employee) ->
    save = if $scope.isPersisted(employee)
      updateEmployee(employee)
    else
      createEmployee(employee)

    save.then ->
      toaster.pop 'success', 'Success!', 'Employee saved'
    .catch (error) ->
      toaster.pop 'error', 'Uh oh!', 'A server error occurred.'
      $log.error(error)
    .finally () ->
      $scope.resetForm()

  $scope.deleteEmployee = (employee) ->
    employee.remove().then ->
      $scope.employees = _.without($scope.employees, employee)

app.controller 'CustomerListCtrl', ($scope, $log, Restangular, toaster) ->
  baseCustomers = Restangular.all('customers')
  $scope.showForm = false
  $scope.currentCustomer = {}

  baseCustomers.getList().then (customers) ->
    $scope.customers = customers

  $scope.deleteCustomer = (customer) ->
    customer.remove().then ->
      $scope.customers = _.without($scope.customers, customer)

  $scope.isPersisted = (customer) ->
    customer.id?

  $scope.resetForm = ->
    $scope.customerForm.$setPristine()
    $scope.currentCustomer = {}
    $scope.showForm = false

  $scope.editCustomer = (customer) ->
    $scope.currentCustomer = customer
    $scope.showForm = true

  createCustomer = (customer) ->
    baseCustomers.post(customer)
    .then (p) -> $scope.customers.push(p)

  updateCustomer = (customer) ->
    customer.save()

  $scope.saveCustomer = (customer) ->
    save = if $scope.isPersisted(customer)
      updateCustomer(customer)
    else
      createCustomer(customer)

    save.then ->
      toaster.pop 'success', 'Success!', 'Customer saved'
    .catch (error) ->
      toaster.pop 'error', 'Uh oh!', 'A server error occurred.'
      $log.error(error)
    .finally () ->
      $scope.resetForm()
