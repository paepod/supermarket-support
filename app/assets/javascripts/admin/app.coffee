###
  Administration application

  Created by Chris Atkins
###
app = angular.module 'adminApp', [
  'restangular'
  'admin.controllers'
  'admin.directives'
  'shared.directives'
  'checklist-model'
]

app.config (RestangularProvider) ->
  RestangularProvider.setBaseUrl '/api'
