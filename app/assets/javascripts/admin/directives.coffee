###
  Directives for use in the administration applicatin

  Created by Chris Atkins
###

app = angular.module('admin.directives', [])

app.directive 'roleTag', ->
  restrict: 'E'
  template: '<span class="label" ng-class="labelType">{{roleName}}</span>&nbsp;'

  link: (scope, elem, attrs) ->
    labelClass = if scope.roleName is 'Manager' then 'info' else 'success'
    scope.labelType = "label-#{labelClass}"

  scope:
    roleName: '='
