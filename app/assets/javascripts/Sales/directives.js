/**
 * Created by James on 16/05/2014.
 *
 * angular js directive used to calculate the height of an element
 */

angular.module('sales.directives', [])

    .directive('heightToBottomLessVal', ['$timeout' ,function($timeout) {
        return {
            restrict: 'A',
            scope: {
                val: '='
            },
            link: function (scope, element, attrs) {
                $timeout(function(){
                    var pos = element.offset();
                    var windowH = $(window).height();
                    element.css("height", windowH - pos.top - parseInt(scope.val));
                    element.css("overflow-y", "auto");
                }, 0);
            }
        }
    }])

;
