/**
 * Created by James on 16/05/2014.
 *
 * the angular Js controller used on the sales page
 */

angular.module('sales.controllers', [])

.controller('ProductListCtrl', ['$scope', 'customerAPI', 'productAPI', 'employeeAPI', 'saleAPI',
        function($scope, customerAPI, productAPI, employeeAPI, saleAPI){

            $scope.getSaleEntryState = function() { return saleState.entry; };
            $scope.getSaleErrorState = function() { return saleState.error; };
            $scope.getSalePaymentState = function() { return saleState.payment; };

            $scope.getQuantityProductType = function() { return productTypes.quantity; };
            $scope.getWeightedProductType = function() { return productTypes.weighted; };

            $scope.getCashPaymentType = function() { return paymentTypes.cash; };
            $scope.getCardPaymentType = function() { return paymentTypes.card; };

            $scope.sale = {
                saleItems:[],
                createdAt: new Date()
            };


            productAPI.getAllProducts().then(function(products){
                $scope.products = products;
            });

            //TODO This will need to tie in with the auth shit to get the current employee
            employeeAPI.getCurrentEmployee().then(function (employee){
                $scope.sale.employee = employee;
            });


            $scope.searchForCustomer = function(customerSearch) {

                customerAPI.searchForCustomer(customerSearch).then(function(customer){
                    $scope.sale.customer = customer;
                }, function (reason) {
                    $scope.customerSearchError = validateCustomerSearch(reason);
                });
                $scope.customerSearch = "";
                $scope.customerSearchError = "";
            };


            $scope.addSaleItem = function (product){
                var saleItem = doesProductExistInSale($scope.sale.saleItems, product);
                if (saleItem != null && product.productType.toLowerCase() === productTypes.quantity) {
                    saleItem.quantity++;
                } else {
                    $scope.sale.saleItems.push(addSmartSaleItem(product));
                }
                $scope.productSearch = "";
            };

            $scope.removeSaleItem = function(saleItem) {
                $scope.sale.saleItems.splice($scope.sale.saleItems.indexOf(saleItem), 1);
            };

            $scope.cancelSale = function() {
                location.reload();
            };

            $scope.acceptPayment = function() {
                var validationMessage = saleValidation.validateSale($scope.sale);
                if (validationMessage !== "") {
                    $scope.showError(validationMessage);
                } else {
                    $scope.sale = calculateSaleTotals($scope.sale);
                    $scope.amountReceived = centsToDollars($scope.sale.totalIncGst);
                    $scope.saleState = saleState.payment;
                }
            };

            $scope.submitSale = function() {
                saleAPI.postSale($scope.sale).then(function (result) {
                    productAPI.deductStockFromSale(result.saleItems).then(function() {
                        $scope.cancelSale();
                        $scope.saleState = saleState.entry;
                    });
                }, function (error) {
                    $scope.showError(saleValidation.putItInAListItem(error.data));
                });
            };

            $scope.showError = function(message) {
                $scope.errorMsg = message;
                $scope.saleState = saleState.error;
            };

            $scope.closeError = function() {
                $scope.errorMsg = "";
                $scope.saleState = saleState.entry;
            };


            //For updating the running total
            $scope.$watch('sale.saleItems', function(newValues, oldValues) {
                $scope.sale.totalExGst = calculateRunningTotal(newValues);
            }, true);

    }])

;