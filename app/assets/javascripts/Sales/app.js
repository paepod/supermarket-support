/**
 * Created by James on 16/05/2014.
 *
 * Angular JS app for the Sales section of the application
 */
angular.module('salesApp', [
    'restangular',
    'sales.controllers',
    'sales.directives',
    'sales.filters',
    'APIServices',
    'ngSanitize'])

    .config(['RestangularProvider', function(RestangularProvider) {
        RestangularProvider.setBaseUrl('/api');
    }])

;
