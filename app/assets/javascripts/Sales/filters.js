/**
 * Created by James on 17/05/2014.
 *
 * angular js filter to filter the products list and sale items
 */
angular.module('sales.filters', [])

    .filter('productFilter', [function() {
        return function(results, selectedProductType, productSearch) {
            if (results && productSearch == "") {
                var tempResults = [];
                angular.forEach(results, function(result) {
                    if (result.productType.toLowerCase() === selectedProductType) {
                        tempResults.push(result);
                    }
                });
                return tempResults;
            } else {
                return results;
            }
        };
    }])

    .filter('productSearch' , [function() {
        return function(results, productSearch) {

            var productSearchActivated = productSearch && productSearch !== "";
            if (results && productSearchActivated) {
                var tempResults = [];
                angular.forEach(results, function(result) {
                    if (result.name.toLowerCase().indexOf(productSearch) > -1) {
                        tempResults.push(result);
                    }
                });
                return tempResults;
            } else {
                return results;
            }
        }
    }])

    .filter('productSaleFilter', [function() {
        return function(results, selectedProductType) {
            if (results) {
                var tempResults = [];
                angular.forEach(results, function(result) {
                    if (result.product.productType.toLowerCase() === selectedProductType) {
                        tempResults.push(result);
                    }
                });
                return tempResults;
            } else {
                return results;
            }
        };
    }])

;