/**
 * Edited by: James Formica
 *
 * add global values for reusablity across the javascript
 * added validation of sales for robustness
 */

var saleState = {
    entry: "entry",
    error: "error",
    payment: "payment"
};

var productTypes = {
    quantity: "quantity",
    weighted: "weighted"
};

var paymentTypes = {
    cash: "cash",
    card: "card"
};

function calculateGST(total) {
    return total / 10;
}

function centsToDollars(cents) {
    return parseFloat((cents / 100).toFixed(2));
}

function doesProductExistInSale(saleItems, product) {
    for (var i = 0, ii = saleItems.length; i < ii; i++) {
        if (saleItems[i].product.id === product.id) {
            return saleItems[i];
        }
    }
    return null;
}



function addSmartSaleItem(product) {
    switch(product.productType.toLowerCase()) {
        case productTypes.quantity :
            return {
                quantity: 1,
                priceOfProduct: product.pricePerItem,
                product: product
            };
        case productTypes.weighted :
            return {
                weight: 0.0,
                priceOfProduct: product.centsPerKilo,
                product: product
            };
    }
}


function validateCustomerSearch(reason) {
    switch (reason.status) {
        case 400:
            return "Please enter a valid Id";
        case 404:
            return reason.data;
        default:
            return "Unexpected error occurred!";
    }
}


function calculateRunningTotal(saleItem) {
    var total = 0.0;
    for (var i = 0, ii = saleItem.length; i < ii; i++) {
        switch (saleItem[i].product.productType.toLowerCase()) {
            case "quantity":
                total += saleItem[i].product.pricePerItem * (saleItem[i].quantity == null ? 1 : saleItem[i].quantity);
                break;
            case "weighted":
                total += saleItem[i].product.centsPerKilo * (saleItem[i].weight == null ? 0 : saleItem[i].weight);
                break;
        }
    }
    return total;
}

function calculateSaleTotals(sale) {
    sale.gst = calculateGST(sale.totalExGst);
    sale.totalIncGst = sale.totalExGst + sale.gst;
    return sale;
}


//Sale Validation
var saleValidation = (function (){

    var validateSale = function (sale) {
        var rtn = "";

        if (!sale.employee){
            rtn += putItInAListItem("No employee assigned");
        }
        if (!sale.customer) {
            rtn += putItInAListItem("No customer assigned");
        }
        if (sale.saleItems.length === 0) {
            rtn += putItInAListItem("No sale items");
        }

        rtn += validateSaleItems(sale.saleItems);

        return rtn;
    };

    var validateSaleItems = function (saleItems) {
        var rtn  = "";
        for (var i = 0, ii = saleItems.length; i < ii; i++) {
            switch(saleItems[i].product.productType.toLowerCase()) {
                case "quantity":
                    rtn += validateQuantityProducts(saleItems[i]);
                    break;
                case "weighted":
                    rtn += validateWeightedProducts(saleItems[i]);
                    break;
            }
        }
        return rtn;
    };

    var validateQuantityProducts = function (saleItem) {
        var rtn = "";
        if(!saleItem.quantity || saleItem.quantity < 1) {
            rtn += putItInAListItem(saleItem.product.name + " needs a quantity");
        }
        return rtn;
    };

    var validateWeightedProducts = function (saleItem) {
        var rtn = "";
        if (!saleItem.weight || saleItem.weight < 0) {
            rtn += putItInAListItem(saleItem.product.name + " needs a weight");
        }
        return rtn;
    };

    var putItInAListItem = function (message) {
        return "<li>" + message + "</li>";
    };

    return {
        validateSale: validateSale,
        putItInAListItem : putItInAListItem
    };
}());







