import com.avaje.ebean.Ebean;
import models.Product;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.Yaml;

import java.util.List;
import java.util.Map;

public class Global extends GlobalSettings  {
    @Override
    public void onStart(Application app) {
        super.onStart(app);

        // Check if the database is empty
        if (Product.find.findRowCount() == 0) {
            seedDatabase();
        }
    }

    @Override
    public void onStop(Application app) {
        super.onStop(app);
        Logger.info("Shutting down!");
    }

    /**
     * Preload initial data set for development from conf/initial-data.yml
     */
    private void seedDatabase() {
        @SuppressWarnings("unchecked")
        Map<String,List<Object>> initialData = (Map<String,List<Object>>) Yaml.load("initial-data.yml");

        Ebean.save(initialData.get("products"));
        Ebean.save(initialData.get("roles"));
        Ebean.save(initialData.get("employees"));
        Ebean.save(initialData.get("customers"));
    }
}
