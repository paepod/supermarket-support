package controllers.authorization;

import play.mvc.With;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to apply authorization rules to a controller class or specific action
 * Inserts RestrictedAction middleware into the request chain.
 *
 * Created by Chris Atkins
 */
@With(RestrictedAction.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RestrictTo {
    String role();
    String message() default "";
}
