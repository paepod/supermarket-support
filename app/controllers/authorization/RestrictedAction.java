package controllers.authorization;

import models.Employee;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.SimpleResult;

import static play.libs.F.Promise;

/**
 * Authorization middleware to restrict controller actions to specific roles.
 * Applied to an action with the RestrictTo annotation
 *
 * Created by Chris Atkins
 */
public class RestrictedAction extends Action<RestrictTo>{
    @Override
    public Promise<SimpleResult> call(Http.Context ctx) throws Throwable {
        Promise<SimpleResult> result = null;

        Employee employee = Secured.getCurrentEmployee();
        if (employee.isInRole(configuration.role())) {
            result = delegate.call(ctx);
        }
        else {
            ctx.flash().put("error", flashMessage());
            result = redirectToHome();
        }

        return result;
    }

    private Promise<SimpleResult> redirectToHome() {
        return Promise.pure(redirect(controllers.routes.Application.index()));
    }

    private String flashMessage() {
        String message;

        if (configuration.message().isEmpty())
            message = "You are not authorized to view this page";
        else
            message = configuration.message();

        return message;
    }
}
