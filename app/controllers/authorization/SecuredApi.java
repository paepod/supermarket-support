package controllers.authorization;

        import play.mvc.Http.Context;
        import play.mvc.Result;

/**
 * Subclass of the Secured middleware for the API
 *
 * Rather than returning a redirect to the home page, it sends back an HTTP 401
 * to be more friendly to API clients
 *
 * Created by Chris Atkins
 */
public class SecuredApi extends Secured {
    @Override
    public Result onUnauthorized(Context ctx) {
        return unauthorized("You must be logged in to access this API action");
    }
}
