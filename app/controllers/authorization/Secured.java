package controllers.authorization;

import models.Employee;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security;

/**
 * Very primative authentication middleware. In a production application would use
 * a proper encrypted session token, or drop in Play plugin
 *
 * Created by Chris Atkins
 */
public class Secured extends Security.Authenticator {
    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("employeeNumber");
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        ctx.flash().put("error", "You must be logged in to access this page.");
        return Results.redirect(controllers.routes.Application.login());
    }

    public static Employee getCurrentEmployee() {
        String employeeNumber = Context.current().request().username();
        return Employee.findByEmployeeNumber(employeeNumber);
    }
}
