package controllers;

import controllers.authorization.RestrictTo;
import controllers.authorization.Secured;
import play.mvc.*;
import views.html.reporting.index;

/**
 * Reporting section controller
 *
 * Just serves the template and restricts access based on role
 *
 * Created by Chris Atkins
 *
 * See app/views/reporting/* and app/assets/javascripts/report/*
 * for front end application created by Shinnawat
 */
@play.mvc.Security.Authenticated(Secured.class)
@RestrictTo(role = "Manager", message = "Reporting section is restricted to managers only.")
public class Reporting extends Controller {

    public static Result index() {
        return ok(index.render());
    }
}
