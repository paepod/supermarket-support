package controllers;

import controllers.authorization.Secured;
import models.Employee;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

/**
 * Root controller for the application serving the home page, and the authentication actions
 *
 * Created by Chris Atkins
 */
public class Application extends Controller {

    @play.mvc.Security.Authenticated(Secured.class)
    public static Result index() {
        return ok(index.render("Home Page"));
    }

    public static Result untrail(String path) {
        return movedPermanently("/" + path);
    }

    public static Result login() {
        return ok(views.html.login.render(Form.form(Login.class)));
    }

    public static Result authenticate() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest(
                views.html.login.render(loginForm)
            );
        } else {
            session().clear();
            session("employeeNumber", loginForm.get().employeeNumber);
            flash("success", "Logged in");
            return redirect(
                routes.Application.index()
            );
        }
    }

    public static Result logout() {
        session().clear();
        flash("success", "Successfully logged out");
        return redirect(
                routes.Application.login()
        );
    }

    /**
     * Login Form POJO
     *
     * Created by Chris Atkins
     */
    public static class Login {
        public String employeeNumber;
        public String password;

        public String validate() {
            if (Employee.authenticate(employeeNumber, password) == null) {
                return "Invalid employee number or password";
            }
            return null;
        }
    }

}
