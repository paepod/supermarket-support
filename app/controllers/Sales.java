package controllers;

import controllers.authorization.RestrictTo;
import controllers.authorization.Secured;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.sales.index;

/**
 * Sales kiosk section controller
 *
 * Just serves the template and restricts access to cashiers.
 *
 * Created by Chris Atkins
 *
 * See app/views/sales/* For templates and app/assets/sales/*
 * for actual sales application created by James Formia
 */
@play.mvc.Security.Authenticated(Secured.class)
@RestrictTo(role = "Cashier", message = "Point of sale section is restricted to cashiers only.")
public class Sales extends Controller {

    public static Result index() {
        return ok(index.render());
    }
}
