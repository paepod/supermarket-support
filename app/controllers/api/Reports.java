package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import models.Sale;
import models.utils.SDateUtils;
import models.utils.extractStrategies.Extracts;
import models.utils.mappers.SaleReportMapper;
import models.utils.reports.SaleReportBuilder;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import viewModels.SaleReport;
import viewModels.SaleReportForm;

import java.util.List;

/**
 * Created by Shinnawat Viboonsitthichok on 21/05/2014.
 */
public class Reports extends Controller {

    // get /api/reports/
    public static Result index() {

        //ok is HTTP 200 the request was successful
        //Get all sale then convert sale to formal SaleReport with json format
        return ok(Json.toJson(SaleReportMapper.map(Sale.find.all())));
    }

    // post /api/reports/
    @BodyParser.Of(BodyParser.Json.class)
    public static Result query() {
        //extract Json to SaleReportForm (criteria from)
        JsonNode jsonNode = Controller.request().body().asJson();
        SaleReportForm srf = Extracts.getInstance().extractsSaleReport(jsonNode);

        //Require sale StartDate and EndDate
        if (srf.getStartDate() == null || srf.getEndDate() == null) {
            return notFound();
        }
        // Generate Dynamic SaleReport
        List<Sale> sales  = SaleReportBuilder.getInstance()
                .createQuery(SDateUtils.truncatedDate(srf.getStartDate()), SDateUtils.setDateToMidnightTime(srf.getEndDate()))
                .setCustomerName(srf.getCustomerName())
                .setEmployeeName(srf.getEmployeeName())
                .setProductName(srf.getProductName())
                .toList();
        //Map List of SaleReports from List of Sale with json format
        List<SaleReport> saleReports = SaleReportMapper.map(sales);
        return ok(Json.toJson(saleReports));
    }
}