package controllers.api;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.authorization.SecuredApi;
import models.Customer;
import models.utils.EbeanValidator;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * RESTful API controller for customers
 *
 * Created by Chris Atkins
 *
 * Updating functionality by Marlon Atton
 */
@play.mvc.Security.Authenticated(SecuredApi.class)
public class Customers extends Controller {

    // GET /api/customers
    public static Result index() {
        List<Customer> customers = Customer.findAll();
        return ok(Json.toJson(customers));
    }

    // POST /api/customers
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        Customer customer = fetchEmployeeFromJson();

        EbeanValidator validator = new EbeanValidator(customer);

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        customer.save();
        return created(Json.toJson(customer));
    }

    // GET /api/customers/1
    public static Result show(long customerId) {
        Customer customer = Customer.findById(customerId);

        if (customer == null) { return notFound("Customer Id not found"); }

        return ok(Json.toJson(customer));
    }

    // GET /api/customers/abc
    public static Result findByName(String customerName) {
        List<Customer> customers = Customer.findAllByName(customerName);
        if (customers == null || customers.size() == 0) { return notFound("Customer name not found"); }

        if (customers.size() > 1) { return notFound("Search too ambiguous"); }

        return ok(Json.toJson(customers.get(0)));
    }

    // PUT /api/customers/1
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(long customerId) {
        Customer customer = Customer.findById(customerId);

        if (customer == null) return notFound();

        Customer updatedCustomer = fetchEmployeeFromJson();

        if (updatedCustomer.getId() != customerId)
            return badRequest();

        EbeanValidator validator = new EbeanValidator(updatedCustomer);

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        Ebean.update(updatedCustomer);
        return ok(Json.toJson(updatedCustomer));
    }

    // DELETE /api/customers/1
    public static Result destroy(long customerId) {
        Customer customer = Customer.findById(customerId);

        if (customer == null) { return notFound(); }

        customer.delete();

        return ok();
    }

    // helpers
    private static Customer fetchEmployeeFromJson(){
        JsonNode jsonNode = Controller.request().body().asJson();
        return Json.fromJson(jsonNode, Customer.class);
    }

}
