package controllers.api;

import controllers.authorization.SecuredApi;
import models.Role;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * RESTful API controller for role entities
 *
 * Is read only as the application roles are seeded.
 *
 * Created by Chris Atkins
 */
@play.mvc.Security.Authenticated(SecuredApi.class)
public class Roles extends Controller {
    public static Result index() {
        List<Role> roles = Role.findAll();
        return ok(Json.toJson(roles));
    }

    public static Result show(long roleId) {
        Role role = Role.findById(roleId);

        if (role == null) { return notFound(); }

        return ok(Json.toJson(role));
    }

}
