package controllers.api;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.authorization.SecuredApi;
import models.Product;
import models.utils.EbeanValidator;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * RESTful API controller for product entities
 *
 * Created by Chris Atkins
 * Updating functionality created by James Formica
 */
@play.mvc.Security.Authenticated(SecuredApi.class)
public class Products extends Controller {

    // GET /api/products/
    public static Result index() {
        List<Product> products = Product.findAll();
        return ok(Json.toJson(products));
    }

    // POST /api/products/
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        Product newProduct = fetchProductFromJson();
        if (newProduct == null) {
            return badRequest("Invalid product type");
        }

        EbeanValidator validator = new EbeanValidator(newProduct);

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        newProduct.save();
        return created(Json.toJson(newProduct));

    }

    // GET /api/products/1
    public static Result show(Long productId) {
        Product product = Product.findById(productId);

        if (product == null) { return notFound(); }

        return ok(Json.toJson(product));
    }

    // PUT /api/products/1
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(Long productId) {

        Product product = Product.findById(productId);

        if (product == null) { return notFound(); }

        Product updatedProduct = fetchProductFromJson();

        if (updatedProduct.getId() != productId)
            return badRequest();

        Ebean.update(updatedProduct);
        return ok(Json.toJson(product));
    }

    // DELETE /api/products/1
    public static Result destroy(Long productId) {
        Product product = Product.findById(productId);

        if (product == null) return notFound();

        product.delete();
        return ok();
    }

    // Helpers

    private static Product fetchProductFromJson() {
        JsonNode jsonNode = Controller.request().body().asJson();
        return Json.fromJson(jsonNode, Product.class);
    }
}
