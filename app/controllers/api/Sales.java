package controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.authorization.SecuredApi;
import models.Sale;
import models.utils.EbeanValidator;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Restful API controller for Sales
 *
 * Created by James Formica
 */
@play.mvc.Security.Authenticated(SecuredApi.class)
public class Sales extends Controller {

    // GET /api/sales
    public static Result index() throws JsonProcessingException{
        List<Sale> sales = Sale.findAll();
        return ok(Json.toJson(sales));
    }

    // POST /api/sales
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        Sale newSale = fetchSaleFromJson();
        newSale.timestamp();

        if (newSale == null) {
            return badRequest("Invalid sale type");
        }

        EbeanValidator validator = new EbeanValidator(newSale);

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        newSale.save();
        return created(Json.toJson(newSale));
    }

    // GET /api/sales/1
    public static Result show(long saleId) {
        Sale sale = Sale.findById(saleId);

        if (sale == null) return notFound();

        return ok(Json.toJson(sale));
    }

    // PUT /api/sales/1
    public static Result update(long saleId) {
        Sale sale = Sale.findById(saleId);

        if (sale == null) return notFound();

        Sale updatedSale = fetchSaleFromJson();

        if (updatedSale.getId() != saleId)
            return badRequest();

        EbeanValidator validator = new EbeanValidator(updatedSale);

        if (! validator.isValid()) {
            return badRequest(Json.toJson(validator.errors()));
        }

        return ok(Json.toJson(updatedSale));
    }

    // DELETE /api/sales/1
    public static Result destroy(long saleId) {
        Sale sale = Sale.findById(saleId);

        if (sale == null) return notFound();

        sale.delete();
        return ok();
    }

    // helpers
    private static Sale fetchSaleFromJson() {
        JsonNode jsonNode = Controller.request().body().asJson();
        return Json.fromJson(jsonNode, Sale.class);
    }
}
