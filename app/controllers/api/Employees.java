package controllers.api;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.authorization.SecuredApi;
import models.Employee;
import models.utils.EbeanValidator;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * RESTful API controller for employee objects
 *
 * Created by Chris Atkins
 * Updating functionality by Marlon Atton
 */
@play.mvc.Security.Authenticated(SecuredApi.class)
public class Employees extends Controller {

    // GET /api/employees/current
    public static Result current() {
        Employee currentUser = SecuredApi.getCurrentEmployee();
        return ok(Json.toJson(currentUser));
    }


    // GET /api/employees/
    public static Result index() {
        List<Employee> employees = Employee.findAll();
        return ok(Json.toJson(employees));
    }

    // POST /api/employees/
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        Employee employee = fetchEmployeeFromJson();
        employee.updateRoles();
        EbeanValidator validator = new EbeanValidator(employee);

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        employee.save();
        return created(Json.toJson(employee));
    }

    // GET /api/employees/1
    public static Result show(long employeeId) {
        Employee employee = Employee.findById(employeeId);

        if (employee == null) { return notFound(); }

        return ok(Json.toJson(employee));
    }

    // PUT /api/employees/1
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(long employeeId) {
        Employee employee = fetchEmployeeFromJson();

        if (employee == null) return notFound();

        EbeanValidator validator = new EbeanValidator(employee);

        if (employee.getId() != employeeId)
            return badRequest();

        if (! validator.isValid()) {
            return status(422, Json.toJson(validator.errors()));
        }

        Ebean.update(employee);
        return ok(Json.toJson(employee));
    }


    // DELETE /api/employees/1
    public static Result destroy(long employeeId) {
        Employee employee = Employee.findById(employeeId);

        if (employee == null) return notFound();

        employee.delete();
        return ok();
    }

    // Helpers
    private static Employee fetchEmployeeFromJson(){
        JsonNode jsonNode = Controller.request().body().asJson();
        return Json.fromJson(jsonNode, Employee.class);
    }
}
