package controllers;

import controllers.authorization.RestrictTo;
import controllers.authorization.Secured;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.admin.dashboard;

/**
 * Server side controller for the administration section.
 *
 * Restricted to users with the Manager role
 *
 * Created by Chris Atkins
 *
 * See app/views/admin/* and app/assets/javascripts/admin/*
 * for front end application created by Chris Atkins
 */
@play.mvc.Security.Authenticated(Secured.class)
@RestrictTo(role = "Manager", message = "Admin section is restricted to managers only.")
public class Admin extends Controller {
    public static Result index() {
        return ok(dashboard.render());
    }
}
