package models;

import java.util.Locale;

public enum ProductType {
    WEIGHTED,
    QUANTITY;

    public static ProductType fromString(String s) {
        return valueOf(s.toUpperCase(Locale.ENGLISH));
    }
}
