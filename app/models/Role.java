package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.format.Formats;
import play.db.ebean.Model;
import play.data.validation.Constraints;
import javax.persistence.*;
import java.util.List;

/**
 * Model for the various authorization roles.
 *
 * Created by Chris Atkins
 */
@Entity
public class Role extends Model {

    // fields
    @Id
    private Long id;

    @Constraints.Required
    @Formats.NonEmpty
    private String name;

    // accessors / mutators
    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    private List<Employee> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    // @pre.condition(name != null)
    // @pre.condition(name.length > 0)
    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    // @pre.condition(employees != null)
    // @pre.condition(employees.size() > 0)
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    // finders
    public static Finder<Long, Role> find =
            new Finder<Long, Role>(Long.class, Role.class);

    // @post.condition(result != null)
    public static List<Role> findAll() {
        return find.all();
    }

    public static Role findById(long roleId) {
        return find.byId(roleId);
    }
}
