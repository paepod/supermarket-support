package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Domain model for line items in a sale transaction
 *
 * Created by Chris Atkins,
 * Extended by James Formica
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class SaleItem extends Model {

    // fields
    @Id
    private Long id;

    @ManyToOne
    private Product product;

    private Long quantity;

    private Double weight;

    // price at time of sale
    private Double priceOfProduct;

    // accessors / mutators
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    // @pre.condition(product != null)
    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    // @pre.condition(quantity > 0)
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getWeight() {
        return weight;
    }

    // @pre.condition(quantity > 0)
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPriceOfProduct() {
        return priceOfProduct;
    }

    // @pre.condition(priceOfProduct > 0)
    public void setPriceOfProduct(Double priceOfProduct) {
        this.priceOfProduct = priceOfProduct;
    }

    // finders
    public static Finder<Long, SaleItem> find =
            new Finder<Long, SaleItem>(Long.class, SaleItem.class);

    public static SaleItem findById(Long saleItemId) {
        return find.byId(saleItemId);
    }

    // @post.condition(result != null)
    public static List<SaleItem> findAll() {
        return find.all();
    }
}
