package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Domain model for recording customer details and tracking sales etc.
 *
 * Created by Chris Atkins
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class Customer extends Model {

    // fields
    @Id
    private Long id;

    @Constraints.Required
    @Formats.NonEmpty
    private String name;

    @Constraints.Required
    @Formats.NonEmpty
    private String customerNumber;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Sale> sales;

    // accessors / mutators
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @JsonIgnore
    public List<Sale> getSales() {
        return sales;
    }

    public void setSales(List<Sale> sales) {
        this.sales = sales;
    }

    // finders
    public static Finder<Long, Customer> find =
            new Finder<Long, Customer>(Long.class, Customer.class);

    // @post.condition(result != null)
    public static List<Customer> findAll() {
        return find.all();
    }

    public static Customer findById(Long id) {
        return find.byId(id);
    }

    public static List<Customer> findAllByName(String name) {
        return find.where().ilike("name", "%"+name+"%").findList();
    }
}
