package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Domain model for a sale transaction
 *
 * Created by Chris Atkins and James Formica
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sale extends Model {

    // fields
    @Id
    private Long id;

    @ManyToOne
    @Constraints.Required
    private Employee employee;

    @ManyToOne
    @Constraints.Required
    private Customer customer;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    private Date createdAt;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonProperty("saleItems")
    private List<SaleItem> items;

    @JsonProperty("totalExGst")
    private Long totalExTax;

    @JsonProperty("totalIncGst")
    private Long totalIncTax;

    // accessors / mutators
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    // @pre.condition(employee != null)
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    // @pre.condition(customer != null)
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    // @pre.condition(createdAt != null)
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<SaleItem> getItems() {
        return items;
    }

    public void setItems(List<SaleItem> items) {
        this.items = items;
    }

    public Long getTotalExTax() { return totalExTax; }

    // @pre.condition(totalExTax >= 0)
    public void setTotalExTax(Long totalExTax) { this.totalExTax = totalExTax; }

    public Long getTotalIncTax() { return totalIncTax; }

    // @pre.condition(totalIncludingTax >= 0)
    public void setTotalIncTax(Long totalIncTax) { this.totalIncTax = totalIncTax; }

    // methods
    public void timestamp() {
        this.createdAt = new Date();
    }

    // finders
    public static Finder<Long, Sale> find =
            new Finder<Long, Sale>(Long.class, Sale.class);

    // @post.condition(result != null)
    public static List<Sale> findAll() {
        return find.all();
    }

    public static Sale findById(Long saleId) {
        return find.byId(saleId);
    }
}
