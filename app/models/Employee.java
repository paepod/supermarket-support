package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Domain model for employees. These are the end users of the application.
 *
 * Created by Chris Atkins
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee extends Model {

    // fields
    @Id
    private Long id;

    @Constraints.Required
    @Formats.NonEmpty
    private String name;

    @Constraints.Required
    @Formats.NonEmpty
    private String employeeNumber;

    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private List<Role> roles;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Sale> sales;

    // accessors / mutators
    public Long getId() {
        return id;
    }

    // @pre.condition(id != null)
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    // @pre.condition(name != null)
    // @pre.condition(name.length() > 0)
    public void setName(String name) {
        this.name = name;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    // @pre.condition(employeeNumber != null)
    // @pre.condition(employeeNumber.length() > 0)
    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public List<Role> getRoles() {
        return roles;
    }

    // @pre.condition(roles != null && ! roles.isEmpty())
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    public List<Sale> getSales() {
        return sales;
    }

    // @pre.condition(sales != null)
    public void setSales(List<Sale> sales) {
        this.sales = sales;
    }

    // finders
    public static Finder<Long, Employee> find =
            new Finder<Long, Employee>(Long.class, Employee.class);

    // @post.condition(result != null)
    public static List<Employee> findAll() {
        return find.all();
    }

    public static Employee findById(long employeeId) {
        return find.byId(employeeId);
    }

    public static Employee findByEmployeeNumber(String employeeNumber) {
        return find.where().eq("employeeNumber", employeeNumber).findUnique();
    }

    // pretend auth for demo purposes
    // in production would use cryptographically strong and salted  hashes
    // from bcrypt or the like
    public static Employee authenticate(String employeeNumber, String password) {
        if (! password.equals("rmit")) {
            return null;
        }
        return findByEmployeeNumber(employeeNumber);
    }

    // methods
    @Override
    public void delete() {
        emptyRoles();
        super.delete();
    }

    // @pre.condition(roleName != null)
    public boolean isInRole(String roleName) {
        for (Role role : roles) {
            if (role.getName().equals(roleName)) {
                return true;
            }
        }
        return false;
    }

    // fiddly workaround for updating the associations from creates and updates
    public void updateRoles() {
        List<Role> updatedRoles = this.roles;
        this.roles = new ArrayList<Role>();

        // Refetch roles from the database
        for(Role role : updatedRoles) {
            this.roles.add(Role.find.byId(role.getId()));
        }
    }

    // helpers

    /**
     * Need to clear the join table rows before deleting
     */
    private void emptyRoles() {
        this.roles.clear();
        this.deleteManyToManyAssociations("roles");
    }
}
