package models;

import play.data.validation.Constraints;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Domain model for products that are sold by quantity.
 *
 * Created by Chris Atkins
 */
@Entity
@DiscriminatorValue(value = "quantity")
public class QuantityProduct extends Product {

    // fields
    @Constraints.Required(message = "Quantity is required")
    @Constraints.Min(value = 0, message = "Cannot have negative stock levels")
    private int quantityInStock;

    @Constraints.Required(message = "Price  is required")
    @Constraints.Min(value = 0, message = "Cannot have negative price")
    private Integer pricePerItem;

    // accessors / mutators
    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    // @pre.condition(quantityInStock > 0)
    // @pre.condition(quantityInStock != null)
    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public Integer getPricePerItem() {
        return pricePerItem;
    }

    // @pre.condition(pricePerItem >= 0)
    // @pre.condition(pricePerItem != null)
    public void setPricePerItem(Integer pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

    @Override
    public ProductType getProductType() {
        return ProductType.QUANTITY;
    }

    @Override
    public String getPriceInfo() {
        double price = pricePerItem * 0.01;
        return String.format("$%02.02f/ea", price);
    }

    @Override
    public String getStockInfo() {
        return String.format("%d units", quantityInStock);
    }
}
