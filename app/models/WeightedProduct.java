package models;

import play.data.validation.Constraints;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue(value = "weighted")
public class WeightedProduct extends Product {

    // fields
    @Constraints.Required(message = "Kilos in stock are required")
    @Constraints.Min(value = 0, message = "Cannot have negative stock levels")
    private BigDecimal kilosInStock;

    @Constraints.Required(message = "Cents per kilo are required")
    @Constraints.Min(value = 0, message = "Cannot have negative price")
    private Integer centsPerKilo;

    // accessors / mutators
    public BigDecimal getKilosInStock() {
        return kilosInStock;
    }

    // @pre.condition(kilosInStock >= 0)
    // @pre.condition(kilosInStock != null)
    public void setKilosInStock(BigDecimal kilosInStock) {
        this.kilosInStock = kilosInStock;
    }

    public Integer getCentsPerKilo() {
        return centsPerKilo;
    }

    // @pre.condition(centsPerKilo > 0)
    // @pre.condition(centsPerKilo != null)
    public void setCentsPerKilo(Integer centsPerKilo) {
        this.centsPerKilo = centsPerKilo;
    }

    @Override
    public ProductType getProductType() {
        return ProductType.WEIGHTED;
    }

    @Override
    public String getPriceInfo() {
        double price = centsPerKilo * 0.01;
        return String.format("$%02.02f/kg", price);
    }

    @Override
    public String getStockInfo() {
        return String.format("%02.02f kg", kilosInStock);
    }
}
