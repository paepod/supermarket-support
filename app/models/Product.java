package models;

import com.fasterxml.jackson.annotation.*;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import java.util.List;

/**
 * Abstract base class to represent various types of products
 * - Uses single table inheritance
 *
 * Created by Chris Atkins
 *   - single table inheritance, some fields
 * Extended by James Formica
 *   - cleaned up the Json functionality to avoid switching on type in most cases
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "productType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = QuantityProduct.class, name = "QUANTITY"),
        @JsonSubTypes.Type(value = WeightedProduct.class, name = "WEIGHTED")
})
public abstract class Product extends Model {
    // fields
    @Id
    private Long id;

    @Constraints.Required(message = "Name is required")
    @Formats.NonEmpty
    private String name;

    @Constraints.Required(message = "Barcode is required")
    @Formats.NonEmpty
    private String barcode;

    // accessors / mutators
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    // @pre.condition(name.length() > 0)
    public void setName(String name) {
        this.name = name;
    }

    // @pre.condition(barcode.length() > 0)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @JsonProperty(value = "productType")
    public abstract ProductType getProductType();

    public abstract String getPriceInfo();

    public abstract String getStockInfo();

    // finders
    public static Finder<Long, Product> find =
            new Finder<Long, Product>(Long.class, Product.class);

    // @post.condition(result != null)
    public static List<Product> findAll() {
        return find.all();
    }

    public static Product findById(Long id) {
        return find.byId(id);
    }
}
