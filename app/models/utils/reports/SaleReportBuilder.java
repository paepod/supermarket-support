package models.utils.reports;

import com.avaje.ebean.Query;
import models.Sale;
import models.utils.StringUtils;
import java.util.Date;
import java.util.List;

/**
 * Dynamic Sale Report builder that will get all values basing on criteria
 * Created by Shinnawat Viboonsitthichok on 17/05/2014.
 */
public class SaleReportBuilder {

    private boolean isCreateQuery = false;
    private Query<Sale> query;
    private static SaleReportBuilder instance;

    private SaleReportBuilder() {

    }

    public static synchronized SaleReportBuilder getInstance() {
        if (instance == null) {
            instance = new SaleReportBuilder();
        }
        return instance;
    }
    //@pre.condition startDate >= endDate
    public SaleReportBuilder createQuery(Date startDate,Date endDate) {
        isCreateQuery = true;
        String oql = "find  sale fetch customer fetch employee fetch items "
                + " where ( created_at >= :startDate and created_at <= :setDateToMidnighTime) ";
        query = Sale.find.setQuery(oql);
        query.setParameter("startDate", startDate);
        query.setParameter("setDateToMidnighTime", endDate);
        return this;
    }

    //@pre.condition isCreateQuery == true;
    public SaleReportBuilder setEmployeeName(String employeeName) {
        if (!StringUtils.isNullOrWhiteSpace(employeeName)) {
            query.where("and ( lower(employee.name) like :employeeName )");
            query.setParameter("employeeName", wrap(employeeName.toLowerCase()));
        }
        return this;
    }
    //@pre.condition isCreateQuery == true;
    public SaleReportBuilder setCustomerName(String customerName) {
        if (!StringUtils.isNullOrWhiteSpace(customerName)) {
            query.where("and ( lower(customer.name) like :customerName )");
            query.setParameter("customerName", wrap(customerName.toLowerCase()));
        }
        return this;
    }
    //@pre.condition isCreateQuery == true;
    public SaleReportBuilder setProductName(String productName) {
        if (!StringUtils.isNullOrWhiteSpace(productName)) {
            query.where("and ( lower(items.product.name) like :productName )");
            query.setParameter("productName", wrap(productName.toLowerCase()));
        }
        return this;
    }

    //@pre.condition isCreateQuery == true;
    public List<Sale> toList(){
        return query.findList();
    }

    //@pre.condition isCreateQuery == true;
    public Query<Sale> getQuery() {
        return this.query;
    }

    private static String wrap(String str) {
        return "%" + str + "%";
    }
}