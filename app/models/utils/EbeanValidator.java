package models.utils;

import play.data.validation.Validation;
import play.db.ebean.Model;

import javax.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Friendlier interface to Ebean validations.
 * Can get a map of errors which is cleaner to convert to JSON
 *
 * Created by Chris Atkins
 */
public class EbeanValidator {
    private final Model subject;

    public EbeanValidator(Model subject)
    {
        this.subject = subject;
    }

    public boolean isValid() {
        return errors().isEmpty();
    }

    public Map<String, String> errors() {
        Map<String, String> errorMap = new HashMap<String, String>();

        for (ConstraintViolation<Model> error : validate()) {
            errorMap.put(error.getPropertyPath().toString(), error.getMessage());
        }

        return errorMap;
    }

    private Set<ConstraintViolation<Model>> validate() {
        return Validation.getValidator().validate(subject);
    }
}
