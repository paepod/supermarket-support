package models.utils;

/**
 * Created by Shinnawat Viboonsitthichok on 24/05/2014.
 */
public class StringUtils {

    /*
     * White-space characters are defined by the Unicode standard.
     * The IsNullOrWhiteSpace method interprets any character
     * that returns a value of true when it is passed to
     * the Character.IsWhiteSpace method as a white-space character.
     * This method offers superior performance
     * @param  string str
     * @return true if string is either null or whiteSpace, otherwise false
     */
    public static boolean isNullOrWhiteSpace(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }

        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}