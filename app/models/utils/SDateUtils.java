package models.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Shinnawat Viboonsitthichok on 24/05/2014.
 */
public class SDateUtils {

    /**
     * set this date to 23:59:59
     * For example, if you had the datetime of 28 Mar 2002 13:45:01.231,
     * it would return 28 Mar 2002 23:59:59.000
     * @param  date - the date to work with Date
     * @return date at midnight time
     * @throws  IllegalArgumentException - if any argument is null
     */
    public static Date setDateToMidnightTime(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("date should not be null");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.clear(Calendar.MILLISECOND);
        return calendar.getTime();
    }

    /**
     * Truncate this date
     * For example, if you had the datetime of 28 Mar 2002 13:45:01.231,
     * it would return 28 Mar 2002 00:00:00.000.
     *
     * @param  date - the date to work with Date
     * @return the rounded date
     * @throws  IllegalArgumentException - if any argument is null
     */
    public static Date truncatedDate(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("date should not be null");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        return calendar.getTime();
    }
}