package models.utils.mappers;

import models.ProductType;
import models.Sale;
import models.SaleItem;
import viewModels.SaleReport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * Created by Shinnawat Viboonsitthichok on 24/05/2014..
 */
public class SaleReportMapper{

    /*
     * map Sale Domain model class to SaleReport View model class/*
     * @param  Collection of Sale (List , Set)
     * @return List of SaleReports
     */
    public static List<SaleReport> map(Collection<Sale> sales) {
        List<SaleReport> reports = new ArrayList<SaleReport>();
        for (Sale sale : sales) {
            for (SaleItem item : sale.getItems()) {
                SaleReport sr = new SaleReport();
                sr.setSaleId(sale.getId());
                sr.setEmployeeId(sale.getEmployee().getId());
                sr.setEmployeeName(sale.getEmployee().getName());
                sr.setEmployeeNumber(sale.getEmployee().getEmployeeNumber());
                sr.setEmployeeRoles(sale.getEmployee().getRoles());
                sr.setCustomerId(sale.getCustomer().getId());
                sr.setCustomerName(sale.getCustomer().getName());
                sr.setCustomerNumber(sale.getCustomer().getCustomerNumber());
                sr.setItems(item);
                sr.setSaleDate(sale.getCreatedAt());
                sr.setSaleId(sale.getId());
                if (item.getProduct().getProductType() == ProductType.WEIGHTED) {
                    sr.setSalePrice(salePrice(item.getPriceOfProduct(), item.getWeight()));
                }
                if (item.getProduct().getProductType() == ProductType.QUANTITY) {
                    sr.setSalePrice(salePrice(item.getPriceOfProduct(), item.getQuantity()));
                }
                reports.add(sr);
            }
        }
        return reports;
    }

    private static String salePrice(double pricePerItem, long quantity) {
        return salePrice(pricePerItem, (double) quantity);
    }

    private static String salePrice(double pricePerItem, double quantity) {
        double price = pricePerItem * 0.01;
        return String.format("$%02.02f", price * quantity);
    }
}