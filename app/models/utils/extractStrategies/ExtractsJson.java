package models.utils.extractStrategies;
import com.fasterxml.jackson.databind.JsonNode;
/**
 * Interface Extracts JsonNode to specify class.
 * Created by Shinnawat Viboonsitthichok on 21/05/2014.
 */
//@Invarient T type should be models or viewmodel class
public interface ExtractsJson<T> {

    /**
     * extract a JsonNode to a Java value
     *
     * @param jsonNode Json value to extract expected Java type of T
     * @pre.condition JsonNode should not null;
    */
    T extract(JsonNode jsonNode);
}