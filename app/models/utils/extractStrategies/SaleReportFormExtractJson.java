package models.utils.extractStrategies;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import viewModels.SaleReportForm;

/**
 * Extract jsonNode to SaleReportFrom
 * Created by Shinnawat Viboonsitthichok on 21/05/2014.
 */
public class SaleReportFormExtractJson implements ExtractsJson<SaleReportForm> {
    /**
     * extract a JsonNode to a Java value
     *
     * @param jsonNode Json value to extract expected Java type of SaleReportFrom
     * @pre.condition JsonNode should not null;
     */
    @Override
    public SaleReportForm extract(JsonNode jsonNode) {
        return Json.fromJson(jsonNode, SaleReportForm.class);
    }
}