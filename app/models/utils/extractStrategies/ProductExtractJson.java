package models.utils.extractStrategies;

import com.fasterxml.jackson.databind.JsonNode;
import models.Product;
import play.libs.Json;

/**
 * Extract JSONNode to Product
 * Created by Shinnawat Viboonsitthichok on 21/05/2014.
 */
public class ProductExtractJson implements ExtractsJson<Product> {
    /**
     * extract a JsonNode to a Java value
     *
     * @param jsonNode Json value to extract expected Java type of Product
     * @pre.condition JsonNode should not null;
     */
    @Override
    public Product extract(JsonNode jsonNode) {
        return Json.fromJson(jsonNode, Product.class);
    }
}