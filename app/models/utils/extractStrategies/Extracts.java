package models.utils.extractStrategies;

import com.fasterxml.jackson.databind.JsonNode;
import models.Product;
import viewModels.SaleReportForm;

import java.util.HashMap;

/**
 * Extract Strtegies clas to extract Json to Class.
 * Created by Shinnawat Viboonsitthichok on 21/05/2014.
 */
public class Extracts {
    private final String PRODUCT = "PRODUCT";
    private final String SALE_REPORT = "SALE_REPORT";
    private final HashMap<Object, Object> extractStrategies =
            new HashMap<Object, Object>();
    private static Extracts instance;

    private Extracts() {
        extractStrategies.put(this.PRODUCT, new ProductExtractJson());
        extractStrategies.put(this.SALE_REPORT, new SaleReportFormExtractJson());
    }

    public synchronized static Extracts getInstance() {
        if (instance == null) {
            instance = new Extracts();
        }
        return instance;
    }

    //@Pre.condition jsonNode properties should match the Product model.
    public Product extractsProduct(JsonNode jsonNode) {
        return ((ProductExtractJson) this.extractStrategies.get(this.PRODUCT)).extract(jsonNode);
    }
    //@Pre.condition jsonNode properties should match the SaleReportForm viewModel.
    public SaleReportForm extractsSaleReport(JsonNode jsonNode) {
        return ((SaleReportFormExtractJson) this.extractStrategies.get(this.SALE_REPORT)).extract(jsonNode);
    }
}