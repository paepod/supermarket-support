package viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.*;

import java.util.Date;
import java.util.List;

/**
 * View model for SaleReport.
 * These are view model properties for the end report json output.
 *
 * Created by Shinnawat Viboonsithiichok on 21/05/2014.
 */
//IgnoreProperties that aren't map to this view models;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaleReport {
    private Long saleId;
    private Long employeeId;
    private String employeeName;
    private String employeeNumber;
    private List<Role> employeeRoles;
    private Long customerId;
    private String customerName;
    private String customerNumber;
    private SaleItem item;
    private String salePrice;
    private Date SaleDate;


    public SaleReport() {

    }

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public List<Role> getEmployeeRoles() {
        return employeeRoles;
    }

    public void setEmployeeRoles(List<Role> employeeRoles) {
        this.employeeRoles = employeeRoles;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Date getSaleDate() {
        return SaleDate;
    }

    public void setSaleDate(Date saleDate) {
        SaleDate = saleDate;
    }

    public SaleItem getItem() {
        return item;
    }

    public void setItems(SaleItem items) {
        this.item = items;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }
}
